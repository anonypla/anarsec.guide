#let anarsec_article(
  title: none,
  frontimage: none,
  backimage: none,
  lastediteddate: none,
  description: none,
  subtitle: none,
  category: none,
  content
) = {
  
  // format lists
  set list(marker: ([•], [--]))


  // front cover
  page()[
    #place(
      center + top,
      dy: 150pt,
      image(frontimage)
    )
    
    #place(
      center + horizon,
      dy: 40pt,
    )[
      #set text(font: "Jost")
      #text(25pt, title)

      #{ 
        if subtitle != "None" {
          text(18pt, subtitle)
        }
      }
    ]
    
    #place(
      center + bottom
    )[
      #set text(font: "Jost")
      #text(18pt)[Series: #category]
    ]

  ]

  // inside cover
  page()[
    #set align(center + bottom)
    
    #text()[This version of the zine was last edited on #lastediteddate. Visit anarsec.guide to see whether it has been updated since.]
    
    #text()[The dagger symbol #super[†] on a word means that there is a glossary entry for it. Ai ferri corti.]
  ]
  
  // set headings
  show heading.where(level: 1): it => {
    pagebreak(weak: true)
    block(width: 100%)[
      #set align(center)
      #set text(size: 22pt, font: "Jost")
      #text(it.body)
      #v(10pt)
    ]
  }
  show heading.where(level: 2): it => block(width: 100%)[
    #set text(size: 16pt, font: "Jost")
    #text(it.body)
    #v(10pt)
  ]
  show heading.where(level: 3): it => block(width: 100%)[
    #set text(size: 14pt, font: "Jost", weight: "bold")
    #text(it.body)
    #v(10pt)
  ]

  // table of contents
  outline(indent: 20pt, depth: 3)

  // format links
  show link: it => {
    it.body
    if type(it.dest) == "string" {
      if it.dest == "https://anarsec.guide/recommendations" or it.dest == "/recommendations" {
	text[ (#emph()[Recommendations])]
      }
      else if it.dest.starts-with("/recommendations#") or it.dest.starts-with("/recommendations/#") {
        locate(location => {
          let elements = query(label(it.dest.trim("/recommendations#", at: start).trim("/recommendations/#", at: start)), location)
	  text[ (#emph()[Recommendations:] #emph(elements.first().body))] 
        })
      }
      else if it.dest.starts-with("https://") or it.dest.starts-with("http://") {
        footnote[#it.dest.trim("https://", at: start).trim("http://", at: start)]
      }
      else if it.dest.starts-with("/glossary#") or it.dest.starts-with("/glossary/#") {
        locate(location => {
          let elements = query(label(it.dest.trim("/glossary#", at: start).trim("/glossary/#", at: start)), location)
          text[#super[†]]
        })
      }
      else if it.dest.starts-with("/") {
        footnote({text[anarsec.guide] + it.dest})
      }
    }
    else if type(it.dest) == "label" {
      locate(location => {
        let elements = query(it.dest, location)
        text[ (#emph(elements.first().body))]
      })
    }
  }

  // content
  set page(numbering: "1")
  set align(left)
  
  pagebreak(weak: true)
  
  content
  
  set page(numbering: none)

  // back cover
  page()[
    #text()[
        #place(center + horizon, dy: -100pt, block(width: 100%, align(left, par(justify: true, description))))
        
        #place(center + bottom, dy: 20pt, image(height: 250pt, backimage))
    ]
  ]
  
}

// blockquote function ; TODO: remove when typst has a native blockquote function (see https://github.com/typst/typst/issues/105)
#let blockquote(
  content
) = align(center)[
  #block(width: 92%, fill: rgb(230, 230, 230), radius: 4pt, inset: 8pt)[
    #align(left)[
      #text(content)
    ]
  ]
]

