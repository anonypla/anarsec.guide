+++
title="Qubes OS for Anarchists"
date=2023-04-07

[taxonomies]
categories = ["Defensive"]
tags = ["intro", "linux", "windows", "qubes", "intermediate"]

[extra]
blogimage="/images/qubes-os.png"
toc=true
dateedit=2023-05-10
a4="qubes-a4.pdf"
letter="qubes-letter.pdf"
+++
Qubes OS is a security-oriented [operating system](/glossary#operating-system-os) (OS), which means it is an operating system designed from the ground up to be more difficult to hack. This is achieved through [compartmentalization](https://www.qubes-os.org/faq/#how-does-qubes-os-provide-security), where each compartment is called a "qube" (using "virtual machines" — more on that below). All other Linux systems like [Tails](/tags/tails/) are *monolithic*, which means that if a hack succeeds anywhere on the system, it can more easily take over. In Qubes OS, if one qube is compromised, the others remain safe. You can think of using Qubes OS as having many different computers on your desk for different activities, but with the convenience of a single physical machine, a single unified desktop environment, and a set of tools for securely using a number of different qubes together as parts of a unified system.

<!-- more -->

Qubes OS can be configured to force all Internet connections through the [Tor network](/glossary/#tor-network) (like Tails) by using [Whonix](https://www.whonix.org/), which is included by default. Devices (USBs, network devices, microphone and camera) are all strongly isolated and only allowed access when it is explicitly granted. "Disposables" are one-off qubes that self-destruct when shut down. 

# Who is Qubes OS For? 

Given that anarchists are [regularly targeted](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/malware.html) for hacking in repressive investigations, Qubes OS is an excellent choice for us. AnarSec [recommends](/recommendations) Qubes OS for everyday use, and [below](#when-to-use-tails-vs-qubes-os) we compare when it is appropriate to use Tails vs. Qubes OS - both have unique strengths. While Tails is so easy to use that you don't even need to know anything about Linux, Qubes OS is a bit more involved, but still designed to be accessible to users like journalists who don't know much about Linux. 

Even if nothing directly incriminating is done on a computer you use every day, its compromise will still give investigators a field day for [network mapping](https://www.csrc.link/threat-library/techniques/network-mapping.html) - knowing who you talk to and what you talk to them about, what projects you are involved in, what websites you read, etc. Most anarchists use everyday computers for some anarchist projects and to commmunicate with other comrades, so making our personal computers difficult to hack is a reasonable goal for all anarchists.  

# How Does Qubes OS Work? 

Qubes OS is not quite another version of Linux. Rather, it is based on many "[virtual machines](/glossary/#virtual-machine-vm)" running Linux. All of these "virtual machines" are configured to work together to form a cohesive operating system. 

What is a virtual machine? [Virtualization](/glossary/#virtualization) is the process of running a virtual computer *inside* your computer. The virtual machine thinks it's a computer running on real hardware, but it's actually running on abstracted hardware (software that mimics hardware). Qubes OS uses a special program called a hypervisor to manage and run many of these virtual machines simultaneously, on the same physical computer. To simplify things, virtual machines are referred to as qubes. Different operating systems such as Debian, Whonix, Fedora, Windows, etc. can all run together at the same time. The hypervisor strongly isolates each of the qubes from each other. 

![](r4.0-xfce.png)

At the risk of overwhelming you, here is an overview of how Qubes OS is structured. You don't need to memorize any of this to actually use Qubes OS, but it may be helpful to understand the outline of the system before you get started. Each rectangle represents a qube (i.e. a virtual machine). Let's break it down.  

## General Usage 

![](qubes-general.png)

Ignore the grayed out parts of the diagram for now. Daily use of Qubes OS primarily involves interaction with two components:

* **App qubes**. In this example, there are three. #1 is running the Debian operating system, #2 is running Fedora, and #3 is running Whonix. App qubes are where you run applications, store files, and do your work. You can have many isolated App qubes for different activities or purposes. Each App qube is like a complete, self-contained operating system. 


* **Service qubes**. Sys qubes (as in *system*) connect to the Internet and to devices. **sys-usb** manages connected USB devices so that they are only attached to a qube with your permission. **sys-net** is similar to sys-usb, but for network devices. **sys-firewall** is firewall control for all Internet-connected qubes, and is in a separate qube so that if sys-net is compromised, the firewall rules can't be trivially changed. Note that qubes never connect directly to sys-net, but always through sys-firewall. **sys-whonix** forces all network traffic through Tor, and connects to the firewall itself.   

You'll notice that App qube #1 is connected to the Internet, App qube #2 is offline, while App qube #3 is connected to the Internet via Tor and is Disposable. Note that Whonix is actually two qubes: the workstation (App qube #3) and the gateway (sys-whonix). This has the security property that if the workstation is compromised, the gateway (where Tor runs) is not. 

A Disposable qube is a type of App qube that self-destructs when its originating window closes. Note that while Tails uses only memory (when the Persistent Storage feature is not enabled), Qubes OS uses the hard drive, so forensic traces are still possible when using a Disposable. 


## Management Qubes 

![](qubes-arch.png)

Two more components are needed to complete the Qubes OS system:

* **Admin qube**. This is the small, isolated and trusted qube that manages the other qubes. It's very protected because if it's compromised, it's game over. It uses a technology called Xen as the hypervisor. It is also called dom0, which is a Xen naming convention. The Admin qube has no network connectivity and is only used to run the [desktop environment](https://en.wikipedia.org/wiki/Desktop_environment) and [window manager](https://en.wikipedia.org/wiki/Window_manager). 

* **Template qubes**. These are where applications and operating system files live. Templates are where you install and update software. Each App qube is based on a Template qube, but the Template is "read-only" from the App qube's perspective. This means that the more sensitive system files are additionally protected from whatever happens in an App qube - they are not retained between App qube restarts. Multiple App qubes can be based on a single Template, which has the convenient feature that updating one Template will update all App qubes based on that Template.

Another security feature of the Qubes OS structure is that the App qubes don't have direct access to the hardware - only the Admin qube can directly access the hard drive and only the Service qubes can directly access the networking, USB, microphone and camera hardware. 

# When to Use Tails vs. Qubes OS

Qubes includes Whonix by default for when you want to force all connections through Tor. As compared by [Privacy Guides](https://www.privacyguides.org/desktop/#anonymity-focused-distributions) (emphasis added):

> Whonix is meant to run as two virtual machines: a “Workstation” and a Tor “Gateway.” All communications from the Workstation must go through the Tor gateway. **This means that even if the Workstation is compromised by [malware](/glossary/#malware) of some kind, the true IP address remains hidden.**
>
>Tails is great for counter forensics due to amnesia (meaning nothing is written to the disk); however, it is not a [hardened](/glossary#hardening) distribution like Whonix. It lacks many anonymity and security features that Whonix has and gets updated much less often (only once every six weeks). **A Tails system that is compromised by malware may potentially bypass the transparent proxy allowing for the user to be deanonymized.**
>
>Whonix virtual machines may be more leak-proof, however they are not amnesic, meaning data may be recovered from your storage device. By design, Tails is meant to completely reset itself after each reboot. Encrypted persistent storage can be configured to store some data between reboots. 

In order to recover data from a Qubes OS system, the [Full Disk Encryption](/glossary#full-disk-encryption-fde) would still need to be successfully [bypassed](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html) (e.g. by seizing the computer when it is turned on, or using a weak password). If the Tails Persistent Storage feature is in use, any data configured to persist will face the same problem. 

Our recommendation is to use Qubes OS:

* As an everyday computer 
* For opening untrusted files or links. Many anarchist projects require this, such as website moderation, publications, etc. 
* For tasks or workflows where Tails is too restrictive or not applicable.

And to use Tails:

* For writing and submitting communiques 
* For action research 
* For provisioning and connecting to hacking infrastructure 
* For anything else where traces will land you in prison 
* If the learning curve for Qubes OS is too steep

# Getting Started

Qubes OS works best on a laptop with a solid state drive (SSD, which is faster than a hard disk drive, or HDD) and 16GB of RAM. A [hardware compatibility list](https://www.qubes-os.org/hcl/) is maintained where you can see if a specific laptop model will work. If you want to [install HEADS open-source firmware](/posts/tails-best/#to-mitigate-against-remote-attacks) it has [limited compatibility](https://osresearch.net/Prerequisites#supported-devices), so keep that in mind when buying your laptop—we recommend the ThinkPad X230 because it's less complicated to install than other models. The X230 is also the only developer-tested laptop model and is easily found in refurbished computer stores for around $200 USD. See the list of [community-recommended computers](https://forum.qubes-os.org/t/5560) for some other options, and [Best Practices](#hardware-security) for further discussion of hardware security. 

The [installation guide](https://www.qubes-os.org/doc/installation-guide/) will get you started. Do not set up dual boot - another operating system could be used to compromise the Qubes OS. If using the [command line](/glossary/#command-line-interface-cli) is over your head, ask a friend to walk you through it, or first learn the basics of the command line and GPG (required during the [verification step](https://www.qubes-os.org/security/verifying-signatures/)) with [Linux Essentials](/posts/linux/). 

In the post-installation:

* Check the boxes for Whonix qubes, and for updates to happen over Tor.

* The post-installation gives the you option to install only Debian or only Fedora Templates (instead of both), and to use the Debian Template for all sys qubes (the default is Fedora). Whether you choose to use Debian or Fedora for qubes that don't require Tor is up to you. The Privacy Guides project [argue](https://www.privacyguides.org/os/linux-overview/#choosing-your-distribution) that the Fedora software model (semi-rolling release) is more secure than the Debian software model (frozen), but also recommend [Kicksecure](https://www.privacyguides.org/en/os/linux-overview/#kicksecure) (which is based on Debian). See [Best Practices](#post-installation-decisions) for further discussion of this configuration choice. 

* Make sys-net disposable. If you are using Wi-Fi instead of Ethernet, you will need to re-enter the Wi-Fi password after every boot.

The [Getting Started](https://www.qubes-os.org/doc/getting-started/) document is a good overview of most of what you need to know to begin. The [Qubes documentation](https://www.qubes-os.org/doc/) is very thorough, but can be difficult for a new user to navigate. We'll go over some basics here that aren't already covered in the Getting Started link. 

# How to Update 

On Qubes OS, you should NOT be using `apt update` or `apt upgrade` from the command line, which you may be used to from other Linux experiences. As the [documentation](https://www.qubes-os.org/doc/how-to-update/) states, "these bypass built-in Qubes OS update security measures. Instead, we strongly recommend using the Qubes Update tool or its command-line equivalents." The first thing you'll want to do after connecting to the Internet is run Qubes Update. From the docs: 

> you can [...] start the tool manually by selecting it in the Applications Menu under “Qubes Tools.” Even if no updates have been detected, you can use this tool to check for updates manually at any time by selecting “Enable updates for qubes without known available updates,” then selecting all desired items from the list and clicking “Next.”

Updates take a moment to be detected on a new system, so select "Enable updates...", check the boxes for all qubes, and press **Next**. A Whonix window may pop up asking you to do a command line update, but this can be ignored as it will be resolved by the update. Once Qubes Update is complete, reboot. 

# How to Copy and Paste Text

Qubes has a special global clipboard that allows you to copy and paste text between qubes.

1. Press **Ctrl+C** to copy text as normal to the internal clipboard of the source App qube.
2. Press **Ctrl+Shift+C** to copy the contents of the internal clipboard of the source App qube to the global clipboard.
3. Press **Ctrl+Shift+V** in the destination App qube to copy the contents of the global clipboard to the internal clipboard of the destination App qube.
4. Press **Ctrl+V** to paste text as usual from the internal clipboard of the destination App qube.

It's a bit tricky at first, but you'll get the hang of it in no time!

# How to Copy and Move Files 

There is a special tool for moving files and directories (folders) between qubes that requires explicit user permission. As a rule of thumb, only move files from more trusted qubes to less trusted ones. 

From the [docs](https://www.qubes-os.org/doc/how-to-copy-and-move-files/):

>1. Open a file manager in the qube containing the file you wish to copy (the source qube), right-click on the file you wish to copy or move, and select **Copy to Other AppVM**... or **Move to Other AppVM**....
![](copy-to.png)
>2. A dialog box will appear in dom0 asking for the name of the target qube (qube B). Enter or select the desired destination qube name.
![](dom0.png)
>3. If the target qube is not already running, it will be started automatically, and the file will be copied there. It will show up in this directory (which will automatically be created if it does not already exist): `/home/user/QubesIncoming/<source_qube>/<filename>`. If you selected Move rather than Copy, the original file in the source qube will be deleted. (Moving a file is equivalent to copying the file, then deleting the original.)
>
>4. If you wish, you may now move the file in the target qube to a different directory and delete the `/home/user/QubesIncoming/` directory when no longer needed.

# How to Shutdown Qubes 

![](r4.1-widgets.png)

Click on the Domains widget to see which Qubes are currently running and how much memory (RAM) and processing power (CPU) they are using. Each qube uses memory, so when you are done with a qube, you should shut it down to free up the memory it is using. Closing windows isn't  enough - you need to manually shut down each qube when you're done with it. 

![](shutdown.png)

# How to Install Software

While Tails [has a Graphical User Interface](https://tails.boum.org/doc/persistent_storage/additional_software/index.en.html) (GUI) for installing additional software, Qubes OS does not at this time, so new software must be installed from the command line. If you are unfamiliar with the command line or how software works in Linux, see [Linux Essentials](/posts/linux/) to get acquainted. When choosing what additional software to install, keep in mind that being [open-source](/glossary/#open-source) is an essential criteria, but not sufficient to be considered secure. The list of [included software for Tails](https://tails.boum.org/doc/about/features/index.en.html#index1h1) will cover many of your needs with reputable choices. 

Software is installed into Templates, which have network access only for their package manager (apt or dnf). Installing a package requires knowing its name, and all can be browsed using a web browser for both [Debian](http://packages.debian.org/) and [Fedora](https://packages.fedoraproject.org/), or on the command line.  

It is best not to install additional software into the default Template, but rather to install the software into a cloned Template, so as not to unnecessarily increase the attack surface of all App qubes based on the default Template. For example, to install packages for working with documents, which are not included by default in `debian-11`, I clone it first. Go to **Applications menu → Qubes Tools → Qube Manager**. Right click on `debian-11` and select "Clone qube". Name the new Template `debian-11-documents`.   

To install new software, as described in the [docs](https://www.qubes-os.org/doc/how-to-install-software/#installing-software-from-default-repositories): 

>1. Start the template.
>
>2. Start a terminal.
>
>3. Install software as normally instructed inside that operating system, e.g.:
>* Fedora: `sudo dnf install <PACKAGE_NAME>`
>*  Debian: `sudo apt install <PACKAGE_NAME>`
>
>4. Shut down the template.
>
>5. Restart all qubes based on the template.
>
>6. (Recommended) In the relevant qubes’ **Settings → Applications** tab, select the new application(s) from the list, and press **OK**. These new shortcuts will appear in the Applications Menu. (If you encounter problems, see [here](https://www.qubes-os.org/doc/app-menu-shortcut-troubleshooting/) for troubleshooting.)

![](menu.png)

Remember that you should not run `apt update` or `dnf update`. 

Returning to the example above, I would start a terminal in the `debian-11-documents` Template I just cloned, and run `sudo apt install libreoffice-writer mat2 bookletimposer gimp gocryptfs`. Once the installation was complete, I shut down the Template. I could then create or assign a qube to use this Template, and use LibreOffice, etc. Installing software should be the only time most users *need* to use the command line with Qubes OS.

You may want to use software that is not in the Debian/Fedora repositories, which makes things a bit more complicated and also poses a security risk - you must independently assess whether the source is trustworthy, rather than relying on Debian or Fedora. Linux software can be packaged in several ways: deb files (Debian), rpm files (Fedora), AppImages, Snaps and Flatpaks. A [forum post](https://forum.qubes-os.org/t/installing-software-in-qubes-all-methods/9991) outlines your options, and several examples are available in [Encrypted Messaging for Anarchists](/posts/e2ee/). If the software is available on [Flathub](https://flathub.org/home) but not in the Debian/Fedora repositories, you can use [Qube Apps](https://micahflee.com/2021/11/introducing-qube-apps/) - if the Flathub software is community maintained, this is a [security consideration](https://www.kicksecure.com/wiki/Install_Software#Flathub_Package_Sources_Security).

# How to Organize Your Qubes

The next step is to decide how to organize your system - there is much more flexibility here than in a monolithic system like Tails. In general, you should try to use disposables to connect to the Internet whenever possible. Here is our recommended setup for the typical user, which can be tweaked as needed.

After installation, a number of qubes already exist. Click on the Applications Menu to see them all. We are going to delete the following default App qubes because they use the Internet without being disposable: `work`, `personal`, and `untrusted`. Go to **Applications menu → Qubes Tools → Qube Manager**. Right-click and select "Delete qube" for each.  

How the App qubes will be organized, without displaying service qubes or Templates:

![](diagram.png)

* **A vault qube**. This is used for all data storage, because a qube that doesn't need networking shouldn't have it. This qube can be reassigned to the `debian-11-documents` Template so that trusted files can be opened there.  

* **A disposable Whonix-Workstation qube (`whonix-ws-16-dvm`)**.
	* [Remember](#general-usage) - Whonix works by using the Whonix-Workstation Template (`whonix-ws-16`) for the App qube, and the Whonix-Gateway Template (`whonix-gw-16`) for a separate Service qube called `sys-whonix` (not shown in this diagram). Unless you are an advanced user, you should never touch the Whonix-Gateway - all your activity takes place in Whonix-Workstation. When an App qube is disposable, the naming convention is to append `-dvm` for *disposable virtual machine*. 
	* Disposables appear in Applications Menu in a way that can be confusing. You will see two entries for this qube: the **Disposable: whonix-ws-16-dvm** entry, which is where you launch applications from, and the **Template (disp): whonix-ws-16-dvm** entry which is the Template for the disposable (do not use applications from here).
	* You can think of a disposable Whonix-Workstation qube as similar to Tails: system-wide Tor, and deletion after shutdown (without the anti-forensics property, as noted above). 
	* Do not customize the disposable Template at all to resist fingerprinting. 

* **A disposable Debian or Fedora qube**. The default `debian/fedora-dvm` qube (depending on your post-installation decision) is disposable, and is great for web browsing that blocks Tor, such as logging into online banking. 

## Creating Qubes

It's possible to just use the system as is, but let's show you how to create an App qube and a disposable. 

* **A Monero qube**. Say you want to use the Monero wallet for an anarchist project. We'll create a new qube to compartmentalize this activity. Go to **Applications menu → Qubes Tools → Create Qubes VM**
	* **Name**: Project-monero
	* **Color**: Yellow 
	* **Type**: AppVM
	* **Template**: whonix-ws-16
	* **Networking**: sys-whonix
	* Now that the qube exists, [install the Monero wallet into the App qube](https://www.kicksecure.com/wiki/Monero#c-kicksecure-for-qubes-app-qube). Then, in the **Settings → Applications** tab, move Monero Wallet to the Selected column and press **OK**. The shortcut will now appear in the Applications Menu.
	* This App qube is not made disposable - we prefer all networked qubes to be disposable, but a simple setup requires data persistence for the wallet to work properly.  

* **An offline disposable qube**. At the moment, both disposables are networked (with and without Tor). Finally, we will demonstrate how to create a disposable without networking for opening untrusted files (like PDFs and LibreOffice documents). Again, go to **Applications menu → Qubes Tools → Create Qubes VM** 
	* **Name**: debian-11-offline-dvm
	* **Color**: Black
	* **Type**: AppVM
	* **Template**: debian-11-documents
	* **Networking**: none
	* You can also use Fedora. In the new qubes' **Settings → Advanced** tab, under "Other", check "Disposable Template", then press **OK**. You will now see the offline disposable at the top of the Applications Menu - make sure you are working in the disposable, not the disposable Template.
	* Go to **Applications menu → Qubes Tools → Qubes Global Settings**. Set the default disposable Template to `debian-11-offline-dvm`  
	* Now, if a malicious document achieves code execution after being opened, it will be in an empty Qube that has no network and will be destroyed upon shutdown. 

[Qubes Task Manager](https://qubes.3isec.org/tasks.html) is a Graphical User Interface for creating and configuring qubes that would otherwise require advanced command line configuration. Available configurations include:

* **Split-GPG**: GPG keys live in an offline qube and access to them is strictly controlled 
* **Split-SSH**: SSH keys live in an offline qube and access to them is strictly controlled 
* **Mullvad-VPN**: A [VPN](/glossary/#vpn-virtual-private-network) qube using the WireGuard protocol (via Mullvad). Mullvad is one of the few reputable VPN companies - they accept cryptocurrency and also sell [voucher cards](https://mullvad.net/en/blog/2022/9/16/mullvads-physical-voucher-cards-are-now-available-in-11-countries-on-amazon/).
* **sys-VPN**: A VPN qube that uses the OpenVPN protocol
* **split-XMR**: The Monero wallet lives in an offline qube and access to it is strictly controlled. 

If you want your non-Tor qubes to be forced through a VPN, this is the easiest way to set it up. 

By default, App qubes only have 2 GB of private storage. This small amount will fill up quickly - when an App qube is about to run out of space, the Disk Space Monitor widget will alert you. To increase the amount of private storage for any qube, go to the qubes' **Settings → Basic** tab and change the "Private storage max size". This storage won't be used immediately, it's just the maximum that can be used by that qube. 

# How to Use Disposables 

Disposables can be launched from the Applications menu: the disposable is at the top, and the disposable Template is near the bottom. For example, to use a disposable Tor Browser, go to **Application Menu → Disposable: whonix-16-ws-dvm → Tor Browser**. This is how you do all your Tor browsing. If you launch a disposable application, but then want to access the file manager for the same disposable qube, you can do so from the Qubes Domains widget in the top-right corner of the interface. If you were to simply select "Files" from the Applications menu, this would launch another disposable. 

Once you close all the windows of a disposable, the whole disposable is shut down and destroyed. The next time it is started, the disposable will fully reflect the state of its Template. In contrast, an App qube must be shut down manually (using the Qubes Domains widget), and will persist data in the `/home`, `/usr/local`, and `/rw/config` directory. The next time it boots, all locations in the file system of an App qube other than these three directories will reflect the state of its Template. See how [inheritance and persistence](https://www.qubes-os.org/doc/templates/#inheritance-and-persistence) works for Templates, App qubes, and disposables for more information. 

![](disposable.png)

In the file manager of an App qube, right-clicking on certain fle types gives you the **Edit In DisposableVM** and **View In DisposableVM** options. This is exactly how we want to open any untrusted files stored in our vault qube. It will use the default disposable that we set earlier, which is offline. As soon as you close the viewing application, the entire disposable is destroyed. If you have edited the file and saved the changes, the changed file will be saved back to the original app qube, overwriting the original. In contrast, viewing in a disposable is read-only, so if the file does something malicious, it can't write to the App qube you launched it from - this is preferred for files you don't need to edit.  

If your file opens in an application other than the one you want, you'll need to change the default for the disposable Template:

1. Send a file of this type to your disposable Template (in our case, `debian-11-offline-dvm`).  
2. Open the file manager for the disposable Template. 
3. Select the file, right click and select **Properties**. 
4. In the **Open With** tab, select your preferred application for this file type. 
5. Press **Set as default**. 
6. Delete the file from the disposable Template (remember to empty the trash).  
7. Shut down the disposable Template for the change to take effect. 

For PDF files, right-click and select **Convert To Trusted PDF**, and for image files, right-click and select **Convert To Trusted Img**. This will sanitize the file so that it can go from untrusted to trusted. This is accomplished by converting it to images in a disposable and cleaning the metadata. 

Certain types of files in an App qube can be set to open in a disposable by default. However, if I set PDF files to always open in a disposable, this is not failsafe - some files may end in `.pdf`, but in fact be something else. [This guide](https://forum.qubes-os.org/t/opening-all-files-in-disposable-qube/4674) sets all file types to open in a disposable to mitigate this possibility. If you'd still like to set the default to open only PDF files in a disposable, right-click a PDF file and select **Open With Other Application → qvm-open-in-dvm**. 

# How to Use Devices (like USBs) 

To learn how to attach devices, we will format the empty USB or hard drive that you will use for backups. The USB will be attached to an offline disposable to mitigate against [BadUSB attacks](https://en.wikipedia.org/wiki/BadUSB). 

1. Go to **Applications menu → Disposable: debian-11-offline-dvm → Disks**. The disposable will have a name with a random number such as disp4653. If Disks does not exist, make the change in the **Settings → Applications** tab. 

2. The Qubes Devices widget is used to attach a USB drive (or just its partitions) to any qube. Just click on the widget and plug in your USB drive (see the screenshot [above](#how-to-shutdown-qubes)). The new entry will be under "Data (Block) Devices", typically `sys-usb:sda` is the one you want (`sda1` is a partition and would need to be mounted manually). Hover over the entry and attach it to the disposable you just started (in the case of the example above, disp4653). 

3. The empty USB or hard drive should now appear in the Disks application. Format the empty device, and then create a new encrypted partition [as you would in Tails](/posts/tails/#how-to-create-an-encrypted-usb). You can use the same LUKS password that you use for your Qubes OS LUKS because you will need to memorize it to restore from backup and it will contain the same data. 

4. Before removing the USB drive, first eject it using the Qubes Devices widget, which will eject it from the qube. Then go to **Applications menu → sys-usb → Files** and select "Safely Remove Drive" to eject it from the computer. 

Webcams and microphones are considered devices and must be attached to an App qube to be used. 

There are command line instructions for setting up an [external keyboard](https://www.qubes-os.org/doc/usb-qubes/#manual-setup-for-usb-keyboards) or [mouse](https://www.qubes-os.org/doc/usb-qubes/#usb-mice) - we recommend configuring a confirmation prompt. We also recommended to enable a USB keyboard [on a dedicated USB controller](https://www.qubes-os.org/doc/usb-qubes/#qubes-41-how-to-enable-a-usb-keyboard-on-a-separate-usb-controller) to compartmentalize the use of peripherals. 

You don't always need to attach a USB drive to another qube with the Qubes Devices widget - it will also be accessible directly from sys-usb, through the File Manager. You can [copy specific files](#how-to-copy-and-move-files) between the USB and another App qube without having to attach the USB controller to the App qube. After the USB is ejected, restart sys-usb - since it's disposable, it will do the job of sanitizing for another device. 

# How to Backup

Once your qubes are organized the way you want them, you should back up your system. Depending on your needs, we recommend a weekly backup - pick a day of the week and add a reminder to your calendar. We also recommend a redundant backup that is stored off-site and synchronized monthly (to protect against data loss in a [house raid](https://www.csrc.link/threat-library/techniques/house-raid.html)).  

Adapted from the [docs](https://www.qubes-os.org/doc/how-to-back-up-restore-and-migrate/#creating-a-backup):

>1. Go to **Applications menu → Qubes Tools → Backup Qubes**. 
>
>2. Move the VMs that you want to back up to the right-hand Selected column. VMs in the left-hand Available column will not be backed up. You may choose whether to compress backups by checking or unchecking the Compress the backup box. Compressed backups will be smaller but take more time to create. Once you have selected all desired VMs, click Next.
>
>3. Go to **Applications menu → Disposable: debian-11-offline-dvm → Files** to start a file manager in an offline disposable. Plug in the LUKS USB or hard drive you will be backing up to and attach it ([see above for instructions on creating and attaching this drive](#how-to-use-devices-like-usbs)). The drive should now be displayed at **Other Locations** in the file manager. Mount the LUKS partition by entering your password. Create a new directory in it called `backups`.
>
>4. In Backup Qubes, select the destination for the backup: 
>* **Target qube**: select the disposable, named something like disp1217. 
>* **Backup directory**: click **...** to select the newly created folder `backups`. 
>5. Set an encryption passphrase, which can be the same as your Qubes OS user passphrase, because you will need to memorize it to restore from backup, and it will contain the same data. This is dom0, so you won't be able to paste it from a password manager.
>6. Untick "Save settings as default backup profile", and press **Next**. 
>7. Once the backup is complete, test restore your backup. Go to **Applications menu → Qubes Tools → Restore Backup**. DO NOT FORGET to select **Test restore to verify backup integrity (no data actually restored)**. A test restore is optional but strongly recommended. A backup is useless if you can’t restore your data from it, and you can’t be sure that your backup is not corrupted until you try to restore.

# Whonix and Tor

The Whonix project has its own [extensive documentation](https://www.whonix.org/wiki/Documentation). So does [Kicksecure](https://www.kicksecure.com/wiki/Documentation), on which Whonix is based. When Whonix is used in Qubes OS, it is sometimes referred to as Qubes-Whonix. Whonix can be used on other operating systems, but it's preferable to use it on Qubes OS because of the superior isolation it provides. 

[Multiple default applications](https://www.whonix.org/wiki/Stream_Isolation#List) on a Whonix-Workstation App qube are configured to use unique circuits of the [Tor network](/glossary#tor-network) so that their activity cannot be correlated - this is called [stream isolation](https://www.whonix.org/wiki/Stream_Isolation).

To take advantage of compartmentalization, create separate Whonix-Workstation App qubes for distinct activities/identities, as we did [above](#creating-qubes) for the Project-monero qube. Distinct Whonix-Workstation App qubes are automatically stream isolated. Note that it is considered best practice not to use [multiple Whonix-Workstation App qubes](https://www.whonix.org/wiki/Multiple_Whonix-Workstation#Safety_Precautions) at the same time:

> While multiple Whonix-Workstation are recommended, this is not an endorsement for using them simultaneously! It is safest to only use one Whonix-Workstation at a time and for a single activity. New risks are introduced by running multiple Whonix-Workstation at the same time. For instance, if a single Whonix-Workstation was compromised, it could potentially perform various side channel attacks to learn about running processes in other VMs, and not all of these can be defeated. Depending on user activities, a skilled adversary might be able to correlate multiple Whonix-Workstations to the same pseudonym. 

Tor Browser won't be able to upload files from `/home/user/QubesIncoming/` due to how permissions are set, so you'll need to move files to another location in `/home/user/` to upload them, such as the Downloads directory.

Occasionally, a new version of the Tor Browser will be available before it can be updated using the Qubes Update tool. When this happens, you can [run **Tor Browser Downloader**](https://www.whonix.org/wiki/Tor_Browser#Installation_Process) from the Whonix-Workstation Template (`whonix-ws-16`). As noted in the [docs](https://www.whonix.org/wiki/Tor_Browser#Summary), do NOT run this tool from a disposable Template - the disposable Template will be updated automatically.

# Password Management 

Passwords should be managed by using KeePassXC from the `vault` App qube. If you are not familiar with KeePassXC, you can learn about it in [Tails for Anarchists](/posts/tails/#password-manager-keepassxc). This leaves three passwords to memorize:

1. [LUKS](/glossary/#luks) password (first boot password)
2. User password (second boot password)
3. KeePassXC password

For advice on password quality, see [Tails Best Practices](/posts/tails-best/#passwords). 

# Windows Qubes

It is possible to have [Windows qubes](https://www.qubes-os.org/doc/windows/), although the installation is a bit involved. This allows programs not available for Linux, such as the Adobe Creative Suite programs, to be used from Qubes OS (ideally offline). Installing "cracked" software downloaded from a torrent is not recommended, as these are often malicious. The Adobe Creative Suite can be downloaded from Adobe and then cracked using [GenP](https://www.reddit.com/r/GenP/wiki/redditgenpguides/#wiki_guide_.232_-_dummy_guide_for_first_timers_genp_.28method_1.3A_cc.2Bgenp.29).  

# Best Practices 

There is much more flexibility in how you configure Qubes OS than Tails, but most of the [Tails best practices](/posts/tails-best/) still apply. To summarize, in the order of the Tails article:

* Protecting your identity 
	* Still [clean metadata](/posts/metadata/) from files before you share them. 
	* Compartmentalization is baked into Qubes OS; instead of restarting Tails, use a dedicated qube. 
* Limitations of the Tor network
	* For sensitive activities, don't use Internet connections that could deanonymize you, and prioritize .onion links when available. BusKill is also [available for Qubes OS](https://www.buskill.in/qubes-os/) (and we recommend not obtaining it through the mail).
	* If you might be a target for physical surveillance, consider doing [surveillance detection](https://www.csrc.link/threat-library/mitigations/surveillance-detection.html) and [anti-surveillance](https://www.csrc.link/threat-library/mitigations/anti-surveillance.html) before going to a cafe. Alternatively, use a Wi-Fi antenna from indoors. 
* Reducing risks when using untrusted computers
	* The [verification stage](https://www.qubes-os.org/security/verifying-signatures/) of the Qubes OS installation is equivalent to the [GnuPG verification of Tails](https://tails.boum.org/install/expert/index.en.html).
	* Only attach USBs and external drives to a qube that is disposable and offline. 
	* To mitigate physical attacks on the computer, buy a dedicated laptop from a refurbished store, make the laptop screws [tamper-evident, and use tamper-evident storage](/posts/tamper/).
	* To mitigate remote attacks on the computer, you can use anonymous Wi-Fi and replace the BIOS with [HEADS](https://osresearch.net/). It's not possible to remove the hard drive, and Qubes OS already isolates the Bluetooth interface, camera, and microphone. USBs with secure firmware are less important thanks to the isolation provided by sys-usb, and a USB with a physical write-protect switch is unnecessary because the operating system files are stored on the hard drive (and App qubes don't have write access to their templates). 
* Encryption
	* Passwords: [See above](#password-management) 
	* Encrypted containers: Gocryptfs works the same way, and is useful for a second layer of defense. 
	* Encrypted communication: Use [Cwtch](https://cwtch.im/) for synchronous messaging, and Element for asynchronous messaging. See [Encrypted Messaging for Anarchists](/posts/e2ee/).
* Phishing awareness
	* This is where Qubes OS really shines. Awareness is no longer your only defense - Qubes OS is designed to protect against [phishing](/glossary/#phishing) attacks.  
	* Open attachments in a disposable and offline qube. 
	* Open links in a disposable Whonix-Workstation qube. 

## Post-installation Decisions

During the [post-installation of Qubes OS](#getting-started), you have the option to install only Debian or only Fedora Templates (instead of both). You also have the option to use the Debian Template for all sys qubes (the default is Fedora). Our recommendation is to install only Debian Templates and convert them to [Kicksecure](https://www.privacyguides.org/en/os/linux-overview/#kicksecure). This way, every App qube on your system will be either Whonix or Kicksecure - Kicksecure is significantly more [hardened](/glossary#hardening) than either Debian or Fedora.  

Kicksecure is not currently [available as a Template](https://www.kicksecure.com/wiki/Qubes#Template). To get the Kicksecure Template, clone the Debian Template - follow the [Kicksecure docs for distribution morphing on Qubes OS](https://www.kicksecure.com/wiki/Qubes#Distribution_Morphing). App qubes that require Internet access without Tor can now use the Kicksecure template instead of the Debian Template. We recommend using disposable qubes whenever possible when connecting to the Internet. To create a Kicksecure disposable:

* Go to **Applications menu → Qubes Tools → Create Qubes VM**
	* Name: kicksecure-16-dvm
	* Color: purple
	* Type: AppVM
	* Template: kicksecure-16
	* Networking: default (sys-firewall)
* In the new qubes' **Settings → Advanced** tab, under "Other", check "Disposable Template", then press **OK**. You will now see the disposable at the top of the Applications Menu - make sure you are working in the disposable, not the disposable Template.

Kicksecure is [considered untested](https://www.kicksecure.com/wiki/Qubes#Service_VMs) for sys qubes. If you set all sys qubes to use the Debian Template during the Qubes OS installation, and set sys qubes to be disposable, the Template for `sys-net`, `sys-firewall`, and `sys-usb` will be `debian-11-dvm`. If you want to use disposable Kicksecure for sys qubes:

* Set `sys-net`, `sys-firewall`, and `sys-usb` to use the `kicksecure-16-dvm` Template.

## Hardware Security 

Hardware security is a nuanced subject, with three prominent factors at play for a Qubes OS computer:

* **Root of trust**: A secure element for storing secrets that can be used as a root of trust during the boot process.  
* **Blobs:** Newer hardware comes with [binary blobs](https://en.wikipedia.org/wiki/Binary_blob) that require trusting corporations to do the right thing, while some older hardware is available without binary blobs. 
* **Microcode updates**: Newer hardware gets [microcode](https://en.wikipedia.org/wiki/Microcode) updates to the CPU that (ideally) fix vulnerabilities as they are discovered, while older hardware doesn't after it's considered end-of-life. The [Heads threat model page](https://osresearch.net/Heads-threat-model/#binary-blobs-microcode-updates-and-transient-execution-vulnerabilities) explains why CPU vulnerabilities are important:

	>"With the disclosure of the Spectre and Meltdown vulnerabilities in January 2018, it became apparent that most processors manufactured since the late 1990s can potentially be compromised by attacks made possible because of [transient execution CPU vulnerabilities](https://en.wikipedia.org/wiki/Transient_execution_CPU_vulnerability). [...]  Future not-yet-identified vulnerabilities of this kind is likely. For users of Qubes OS, this class of vulnerabilities can additionally compromise the enforced isolation of virtual machines, and it is prudent to take the risks associated with these vulnerabilities into account when deciding on a platform on which to run Heads and Qubes OS." 

Of the [community-recommended computers](https://forum.qubes-os.org/t/5560), the **ThinkPad X230** and **ThinkPad T430** strike a relatively unique balance because they both use the [Ivy generation](https://en.wikipedia.org/wiki/Ivy_Bridge_(microarchitecture)) of CPUs and are both compatible with Heads:

* **Root of trust**: Heads uses the [Trusted Platform Module (TPM)](https://tech.michaelaltfield.net/2023/02/16/evil-maid-heads-pureboot/#tpm) to store secrets during the boot process - the Thinkpad X230 and T430 have TPM v1.1.  
* **Blobs**: There are no binary blobs on these models after Heads is installed, except for the Intel Management Engine (which can be neutered) and the Ethernet blob (which can be generated). 
* **Microcode updates**: Spectre and Meltdown [are mitigated by microcode updates for this generation of CPUs](https://forum.qubes-os.org/t/secure-hardware-for-qubes/19238/52) which are [installed by default on Qubes OS](https://www.whonix.org/wiki/Spectre_Meltdown#Qubes_2). Newer hardware uses CPUs with different extensions that are vulnerable to new attack vectors - the Ivy generation is not affected by these. 

Qubes OS also applies appropriate software mitigation to this class of attacks at the hypervisor level, including [disabling HyperThreading](https://www.qubes-os.org/news/2018/09/02/qsb-43/).

## OPSEC for Memory Use  

To address "future not-yet-identified vulnerabilities of this kind" on older hardware that no longer receives microcode updates, the operational security (OPSEC) suggestion is to limit the presence of secrets in memory that could lead to leaks. Each running qube uses memory, and a compromised qube could use such vulnerabilities to read and exfiltrate memory used by other qubes. Disposables are reset after they are shut down, so we can assume that their compromise would likely be temporary. Perform sensitive operations in qubes without networking, and shut down secure qubes when not in use. Be aware of which qubes are running simultaneously:  

* [vault qube](#how-to-organize-your-qubes): 
	* Do not run an unlocked KeePassXC database at the same time as a highly untrusted qube. 
	* Instead of having only one vault qube that stores all files (as described above), you can compartmentalize by having different vault qubes dedicated to specific activities (i.e. `vault-personal`, `vault-project1`, etc.). This means that if a networked qube is compromised while working on project1, [intentional sniffing](https://www.qubes-os.org/doc/data-leaks/) will not have potential access to all files, but only to those files that are compartmentalized for project1.
* sys-usb: Disposable. Run only when needed, and shut down when finished. 
* sys-net: Disposable. Run only when needed, and shut down when finished. Shut down when performing sensitive operations in other qubes, if possible. Restart before activities that require sys-net (i.e. email, ssh sessions, etc.).

## Remove Passwordless Root 

By default, Qubes OS does not require a password for root privileges (in other words, you can run a command with `sudo` without a password). The [documentation](https://www.qubes-os.org/doc/vm-sudo/) explains the reasons for this decision. In keeping with the security principle of defense-in-depth, we recommend enabling a password for root privileges. Forcing an adversary to successfully execute privilege escalation can be a mitigating factor, given the hardening of the Kicksecure/Whonix Templates and the limited time window provided by disposables.  

If you are comfortable with the command line, follow the [docs](https://www.qubes-os.org/doc/vm-sudo/#replacing-passwordless-root-access-with-dom0-user-prompt) for replacing passwordless root access with a Dom0 user prompt in Debian/Whonix/Kicksecure Templates.  

# Wrapping Up

The documentation has several [troubleshooting entries](https://www.qubes-os.org/doc/#troubleshooting), and the [forum](https://forum.qubes-os.org/) is generally very helpful. We recommend that you start using Qubes OS gradually, where you can perform tasks in Qubes OS instead of your previous operating system, as trying to learn everything at once can be overwhelming. 
