+++
title="Tails Best Practices"
date=2023-04-08

[taxonomies]
categories = ["Defensive"]
tags = ["linux", "tails", "easy"]

[extra]
blogimage="/images/tails1.png"
toc=true
dateedit=2023-05-10
a4="tails-best-a4.pdf"
letter="tails-best-letter.pdf"
+++

As mentioned in our [recommendations](/recommendations/#computers), Tails is an [operating system](/glossary#operating-system-os) that is unparalleled for sensitive computer use that requires leaving no forensic trace (writing and sending communiques, research for actions, etc.). Tails runs from a USB drive and is [designed](https://tails.boum.org/about/index.en.html) to leave no trace of your activity on your computer, and to force all Internet connections through the [Tor network](/glossary#tor-network). If you are new to Tails, start with [Tails for Anarchists](/posts/tails/). 

This text describes some additional precautions you can take that are relevant to an anarchist [threat model](/glossary#threat-model). Not all anarchist threat models are the same, and only you can decide which mitigations are worth putting into practice for your activities, but we aim to provide advice that is appropriate for high-risk activities. The [CSRC Threat Library](https://www.csrc.link/threat-library/) is another great resource for thinking through your threat model and appropriate mitigations. 

<!-- more -->

# Tails Warnings

Let's start by looking at the [Tails Warnings page](https://tails.boum.org/doc/about/warnings/index.en.html). 

## Protecting your identity when using Tails

![](identity.png)

> Tails is designed to hide your identity. But some of your activities could reveal your identity:
>
> 1. Sharing files with [metadata](/glossary#metadata), such as date, time, location, and device information
> 2. Using Tails for more than one purpose at a time

### 1. Sharing files with metadata

You can mitigate this first issue by **cleaning metadata from files before sharing them**:

* To learn how, see [Removing Identifying Metadata From Files](/posts/metadata/). 

### 2. Using Tails for more than one purpose at a time

You can mitigate this second issue by what's called **"compartmentalization"**:

* [Compartmentalization](https://www.csrc.link/threat-library/mitigations/compartmentalization.html) means keeping different activities or projects separate. If you use Tails sessions for more than one purpose at a time, an adversary could link your different activities together. For example, if you log into different accounts on the same website in a single Tails session, the website could determine that the accounts are being used by the same person. This is because websites can tell when two accounts are using the same Tor circuit.
* To prevent an adversary from linking your activities while using Tails, restart Tails between different activities. For example, restart Tails between checking different project emails.
* Tails is amnesiac by default, so to save any data from a Tails session, you must save it to a USB. If the files you save could be used to link your activities together, use a different encrypted ([LUKS](/glossary#luks)) USB stick for each activity. For example, use one Tails USB stick for moderating a website and another for researching actions. Tails has a feature called Persistent Storage, but we do not recommend using it for data storage, explained [below](#using-a-write-protect-switch).  

## Limitations of the [Tor network](/glossary#tor-network)

![](tor.png)

> Tails uses the Tor network because it is the strongest and most popular network to protect from surveillance and censorship. But Tor has limitations if you are concerned about:
>
> 1. Hiding that you are using Tor and Tails
> 2. Protecting your online communications from determined, skilled attackers

### 1. Hiding that you are using Tor and Tails

You can mitigate this first issue by [**Tor bridges**](https://tails.boum.org/doc/anonymous_internet/tor/index.en.html#bridges):

* Tor Bridges are secret Tor relays that hide your connection to the Tor network. However, this is only necessary where connections to Tor are blocked, such as in heavily censored countries, by some public networks, or by some parental control software. This is because Tor and Tails don't protect you by making you look like any other Internet user, but by making all Tor and Tails users look the same. It becomes impossible to tell who is who among them.

### 2. Protecting against determined, skilled attackers

An *end-to-end correlation* attack is a theoretical way that a global adversary could break Tor's anonymity:
> A powerful adversary, who could analyze the timing and shape of the traffic entering and exiting the Tor network, might be able to deanonymize Tor users. These attacks are called *end-to-end correlation* attacks, because the attacker has to observe both ends of a Tor circuit at the same time. [...] End-to-end correlation attacks have been studied in research papers, but we don't know of any actual use to deanonymize Tor users.

You can mitigate the techniques available to powerful adversaries by **not using an Internet connection that is tied to your identity**, and by **prioritizing .onion links when available**.

#### Internet not tied to your identity

"Mobile Wi-Fi" devices exist which give you Internet access through the mobile network (via SIM cards) - these are a bad idea. The unique identification number of your SIM card (IMSI) and the unique serial number of your adapter (IMEI) are also transmitted to the mobile operator every time you connect, allowing identification and geographic localization. The adapter works like a mobile phone! If you do not want different research sessions to be associated with each other, do not use the same device or SIM card more than once!

Use an Internet connection that isn't connected to you, such as in a cafe without CCTV cameras. There are several opsec considerations to keep in mind when using Wi-Fi in a public space like this. 
* See [below](#appendix-2-location-location-location) for more information on choosing a location.  
* Do not get into a routine of using the same cafes repeatedly if you can avoid it. 
* If you have to buy a coffee to get the Wi-Fi password, pay in cash! 
* Position yourself with your back against a wall so that no one can "shoulder surf" to see your screen, and ideally install a [privacy screen](/posts/tails/#privacy-screen) on your laptop. 
* Maintain situational awareness and be ready to pull out the Tails USB to shut down the computer at a moment's notice. If maintaining situational awareness seems unrealistic, consider asking a trusted friend to hang out who can dedicate themselves to keeping an eye on your surroundings. If the Tails USB is removed, Tails will shut down and [overwrite the RAM with random data](https://tails.boum.org/doc/advanced_topics/cold_boot_attacks/index.en.html). Any LUKS USBs that were unlocked in the Tails session will now be encrypted again. Note that [Tails warns](https://tails.boum.org/doc/first_steps/shutdown/index.en.html) "Only physically remove the USB stick in case of emergency as doing so can sometimes break the file system of the Persistent Storage." 
	* One person in charge of a darknet marketplace had his Tails computer seized while distracted by a fake fight next to him. Similar tactics have been used [in other police operations](https://dys2p.com/en/2023-05-luks-security.html#attacks). If his Tails USB had been attached to a belt with a short piece of fishing line, the police would most likely have lost all evidence when the Tails USB was pulled out. A more technical equivalent is [BusKill](https://www.buskill.in/tails/) - however, we only recommend buying this in person (such as at a conference) or [3D printing it](https://www.buskill.in/3d-print-2023-08/). This is because any mail can be [intercepted](https://docs.buskill.in/buskill-app/en/stable/faq.html#q-what-about-interdiction) and altered, making the hardware [malicious](https://en.wikipedia.org/wiki/BadUSB). 
* If coffee shops without CCTV cameras are few and far between, you can try accessing a coffee shop's Wi-Fi from outside, out of view of the cameras. Some external Wi-Fi adapters can pick up signals from further away, as discussed [below](#appendix-2-location-location-location). 

#### Non-Targeted and Targeted Correlation Attacks

As described in the quotation above, a global adversary (i.e. the NSA) may be capable of breaking Tor through a [correlation attack](https://anonymousplanet.org/guide.html#your-anonymized-torvpn-traffic). If this happens, the Internet address you used in a coffee shop without CCTV cameras will only lead to your general area (e.g. your city) because it is not associated with you. Of course, this is less true if you use it routinely. Correlation attacks are even less feasible against connections to an .onion address because you never leave the Tor network, so there is no "end" to correlate with through network traffic analysis (if the server location is unknown to the adversary).  

What we will term a "targeted" correlation attack is possible by a non-global adversary (i.e. local law enforcement), if you are already in their sights and a target of [physical surveillance](https://www.csrc.link/threat-library/techniques/physical-surveillance/covert.html) and/or [digital surveillance](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance.html). This is a subtype of correlation attack where the presumed target is already known, thus making the attack easier to achieve because it vastly reduces the amount of data to filter through for correlation. A non-targeted correlation attack used to deanonymize a Tor user is unprecedented in current evidence used in court, although [a "targeted" correlation attack has been used](https://medium.com/beyond-install-tor-signal/case-file-jeremy-hammond-514facc780b8) as corroborating evidence - a suspect had already been identified, which allowed investigators to correlate their local footprint with specific online activity. Specifically, they correlated Tor network traffic coming from the suspect's house with the times their anonymous alias was online in chatrooms. 

To explain how this works, it helps if you have a basic understanding of what Tor information is visible to various third parties - see the EFF's [interactive graphic](https://www.eff.org/pages/tor-and-https). For a non-targeted correlation attack, the investigator will need to **start from after Tor's exit node**: take the specific online activity coming from the exit node and try to correlate it with an enormous amount of global data that is entering Tor entry nodes. However, if a suspect is already identified, the investigator can instead do a "targeted" correlation attack and **start from before Tor's entry node**: take the data entering the entry node (via **the suspect's physical or digital footprint**) and try to correlate it with **specific online activity** coming from the exit node. 

A more sophisticated analysis of the specific online activity would involve logging the connections to the server for detailed comparison, and a simple analysis would be something that is publicly visible to anyone (such as when your alias is online in a chatroom, or when a post is published to a website). For your physical footprint, a surveillance operation can note that you go to a cafe regularly, then try to correlate this with online activity they suspect you of (for example, if they suspect you are a website moderator, they can try to correlate these time windows with web moderator activity). For your digital footprint, if you are using Internet from home, an investigator can log all your Tor traffic and then try to correlate it with specific online activity.

* Possible mitigations in this scenario include **doing [surveillance detection](https://www.csrc.link/threat-library/mitigations/surveillance-detection.html) and [anti-surveillance](https://www.csrc.link/threat-library/mitigations/anti-surveillance.html) before going to a coffee shop**, and changing Wi-Fi locations regularly. For projects like moderating a website that require daily Internet access, this may not be particularly realistic. In that case, the ideal mitigation is to **use a Wi-Fi antenna from indoors** (guide coming soon) - a physical surveillance effort won't see you entrying a cafe, and a digital surveillance effort won't see anything on your home Internet. If this is too technical for you, you may even want to **use your home internet** for some projects that require very frequent internet access. This contradicts the previous advice to not use your personal Wi-Fi. It's a trade-off: using Tor from home avoids creating a physical footprint that is so easy to observe, at the expense of creating a digital footprint which is more technical to observe, and may be harder to draw meaningful conclusions from (especially if you intentionally [make correlation attacks more difficult](/posts/tails/#make-correlation-attacks-more-difficult)). 
* If you want to submit a report-back the morning after a riot, or a communique shortly after an action (times when there may be a higher risk of targeted surveillance), consider waiting and at least taking surveillance detection and anti-surveillance measures beforehand. In 2010, the morning after a bank arson in Canada, police surveilled a suspect as he traveled from his home to an Internet cafe, and watched him post the communique and then bury the laptop in the woods. More recently, investigators physically surveilling [an anarchist in France](https://www.csrc.link/#quelques-premiers-elements-du-dossier-d-enquete-contre-ivan) installed a hidden camera to monitor access to an Internet cafe near the comrade's home and requested CCTV footage for the day an arson communique was sent.  

To summarize: For highly sensitive activities, use Internet from a random cafe, preceeded by surveillance detection just like you would prior to a direct action. For activities that require frequent internet access such that the random cafe model isn't sustainable, it's best to use a Wi-Fi antenna positioned behind a window to access from a few kilometers away. If this is too technical for you, using your home Wi-Fi is an option, but requires putting faith in it being difficult to break Tor with a non-targeted correlation attack, and it being difficult to draw meaningful conclusions from your home's Tor traffic through a "targeted" correlation attack. 

## Reducing risks when using untrusted computers

![](warning_compromisedpc.png)

> Tails can safely run on a computer that has a virus. But Tails cannot always protect you when:
>
> 1. Installing from an infected computer
> 2. Running Tails on a computer with a compromised BIOS, firmware, or hardware

### 1. Installing from an infected computer

You can mitigate this first issue by **using a computer you trust to install Tails**:

* According to our [recommendations](/recommendations/#computers), this would ideally be a [Qubes OS](/posts/qubes/) system, as it is much harder to infect than a normal Linux computer. If you have a trusted friend with a Tails USB stick that has been installed with Qubes OS (and who uses these best practices), you could [clone it](/posts/tails/#installation) instead of installing it yourself. 
* Use the "Terminal" installation method ["Debian or Ubuntu using the command line and GnuPG"](https://tails.boum.org/install/expert/index.en.html), as it more thoroughly verifies the integrity of the download using [GPG](/glossary/#gnupg-openpgp). If using the [command line](/glossary/#command-line-interface-cli) is over your head, ask a friend to walk you through it, or first learn the basics of the command line and GnuPG with [Linux Essentials](/posts/linux/).
* Once installed, do not plug your Tails USB stick (or any [LUKS](/glossary/#luks) USBs used during Tails sessions) into any other computer while it is running a non-Tails operating system; if the computer is infected, the infection can [spread to the USB](https://en.wikipedia.org/wiki/BadUSB). 

### 2. Running Tails on a computer with a compromised BIOS, firmware, or hardware

This second issue requires several mitigations. Let's start with a few definitions. 

* *Hardware* is the physical computer you are using. 
* *Firmware* is the software that's embedded in a piece of hardware; you can simply think of it as "software for hardware". It can be found in several different places (hard drives, USB drives, graphics processor, etc.). 
* *BIOS* is the specific firmware that is responsible for booting your computer when you press the power button—this is a great place for [malware](/glossary/#malware) to hide because it is undetectable by the operating system. 

Our adversaries have two attack vectors to compromise BIOS, firmware, hardware, or software: [remote attacks](/glossary#remote-attacks) (via the Internet) and [physical attacks](/glossary/#physical-attacks) (via physical access). Not everyone will need to apply all of the advice below. For example, if you're only using Tails for anonymous web browsing and writen correspondence, some of this may be overkill. However, if you're using Tails to take responsibility for actions that are highly criminalized, a more thorough approach is likely relevant. 

#### To mitigate against physical attacks:

> Your computer might be compromised if its physical components have been altered. For example, if a keylogger has been physically installed on your computer, your passwords, personal information, and other data typed on your keyboard could be stored and accessed by someone else, even if you are using Tails.

* First, **get a fresh computer**. A laptop from a random refurbished computer store is unlikely [to already be compromised](https://arstechnica.com/tech-policy/2014/05/photos-of-an-nsa-upgrade-factory-show-cisco-router-getting-implant/). Buy your computer with cash so it cannot be traced back to you, and in person because mail can be intercepted—a used [T Series](https://www.thinkwiki.org/wiki/Category:T_Series) or [X Series](https://www.thinkwiki.org/wiki/Category:X_Series) Thinkpad from a refurbished computer store is a cheap and reliable option. It is best to use Tails with a dedicated laptop, which prevents the adversary from targeting the hardware through a less secure operating system or through your normal non-anonymous activities. Another reason to have a dedicated laptop is that if something in Tails breaks, any information that leaks and exposes the laptop won't automatically be tied to you and your daily computer activities.

![](X230.jpg)

* **Make the laptop's screws tamper-evident, store it in a tamper-evident manner, and monitor for break-ins**. With these precautions in place, you'll be able to detect any future physical attacks. See the [Making Your Electronics Tamper-Evident](/posts/tamper/) tutorial to adapt your laptop's screws, use some form of intrusion detection, and store your laptop so you'll know if it's been physically accessed. Store any external devices you’ll be using with the laptop in the same way (USB, external hard drive, mouse, keyboard). When physical attack vectors are mitigated, an adversary can only use remote attacks. 

#### To mitigate against remote attacks:

* **Wi-Fi that is unrelated to your identity**. We recommend using Wi-Fi that is unrelated to your identity (i.e. not at your home or work) not only to mitigate deanonymization, but also to mitigate remote hacking. It is best to never use the dedicated Tails laptop on your home Wi-Fi. This makes the laptop much less accessible to a remote attacker than a laptop that is constantly connected to your home Wi-Fi. If an attacker is targeting you, they need a point to start, and your home Wi-Fi is a pretty good place to start. 
* **Remove the hard drive**—it's easier than it sounds. If you buy the laptop, you can ask the store to do it and potentially save some money. If you search on youtube for "remove hard drive" for your specific laptop model, there will probably be an instructional video. Make sure you remove the laptop battery and unplug the power cord first. We remove the hard drive to completely eliminate the hard drive firmware, which has been known to be [compromised to install persistent malware](https://www.wired.com/2015/02/nsa-firmware-hacking/). A hard drive is part of the attack surface and is unnecessary on a live system like Tails that runs off a USB. 
* Consider **removing the Bluetooth interface, camera, and microphone** while you're at it, although this is more involved—you'll need the user manual for your laptop model. The camera can at least be "disabled" by putting a sticker over it. The microphone is often connected to the motherboard via a plug - in this case just unplug it. If this is not obvious, or if there is no connector because the cable is soldered directly to the motherboard, or if the connector is needed for other purposes, cut the microphone cable with a pair of pliers. The same method can be used to permanently disable the camera if you don't trust the sticker method. It is also possible to use Tails on a dedicated "offline" computer by removing the network card as well. Some laptops have switches on the case that can be used to disable the wireless interfaces, but for an "offline" computer it is preferable to actually remove the network card.  

* **Replace the BIOS with [HEADS](https://osresearch.net/)**. A [video](https://invidious.sethforprivacy.com/watch?v=sNYsfUNegEA) demonstrates a remote attack on the BIOS firmware against a Tails user, allowing the security researcher to steal GPG keys and emails. Unfortunately, the BIOS cannot be removed like the hard drive. It is needed to turn on the laptop, so it must be replaced with [open-source](/glossary#open-source) firmware. This is an advanced process because it requires opening the computer and using special tools. Most anarchists will not be able to do this themselves, but hopefully there is a trusted person in your networks who can set it up for you. The project is called HEADS because it's the other side of Tails—where Tails secures software, HEADS secures firmware. It has a similar purpose to the [Verified Boot](https://www.privacyguides.org/en/os/android-overview/#verified-boot) found in GrapheneOS, which establishes a full chain of trust from the hardware. HEADS has [limited compatibility](https://osresearch.net/Prerequisites#supported-devices), so keep that in mind when buying your laptop if you plan to install it—we recommend the ThinkPad X230 because it's less involved to install than other models. The CPUs of this generation are capable of effectively removing the [Intel Management Engine](https://en.wikipedia.org/wiki/Intel_Management_Engine#Assertions_that_ME_is_a_backdoor) when flashing HEADS, but this is not the case with later generations of CPUs on newer computers. [Coreboot](https://www.coreboot.org/users.html), the project on which HEADS is based, is compatible with a wider range of laptop models but has less security. HEADS can be configured to [verify the integrity of your Tails USB](https://osresearch.net/InstallingOS/#generic-os-installation), preventing it from booting if it has been tampered with. HEADS protects against physical and remote classes of attacks!

* **Use USBs with secure firmware**, such as the [Kanguru FlashTrust](https://www.kanguru.com/products/kanguru-flashtrust-secure-firmware-usb-3-0-flash-drive), so that the USB will [stop working](https://www.kanguru.com/blogs/gurublog/15235873-prevent-badusb-usb-firmware-protection-from-kanguru) if the firmware is compromised. Kanguru has [retailers worldwide](https://www.kanguru.com/pages/where-to-buy), allowing you to buy them in person to avoid the risk of mail interception.

![](flashtrust.webp)

* **Use a USB with a physical write-protect switch**. 

# Using A Write-Protect Switch

> What's a *write-protect* switch? When you insert a normal USB into a computer, the computer does *read* and *write* operations with it, and a *write* operation can change the data on the USB. Some special USBs developed for malware analysis have a physical switch that can lock the USB, so that data can be *read* from it, but no new data can be *written* to it.

If your Tails USB stick has a write-protect switch and secure firmware, such as [Kanguru FlashTrust](https://www.kanguru.com/products/kanguru-flashtrust-secure-firmware-usb-3-0-flash-drive), you are protected from compromising the USB firmware during a Tails session. If the switch is locked, you are also protected from compromising the Tails software. This is critical. To compromise your Tails USB stick, an attacker would need to be able to write to it. This means that even if a Tails session is infected with malware, Tails itself is immutable, so the compromise cannot "take root" and would not carry over to subsequent Tails sessions. If you are unable to obtain such a USB, you have two options.

1) [Burn Tails to a new DVD-R/DVD+R](https://tails.boum.org/install/dvd/index.en.html) (write once) for each new version of Tails. Don't use DVDs labeled "DVD+RW" or "DVD+RAM", which can be rewritten.
2) Boot Tails with the `toram` option, which loads Tails completely into memory. Using the `toram` option depends on whether your Tails USB boots with [SYSLINUX or GRUB](https://tails.boum.org/doc/advanced_topics/boot_options/index.en.html).
	* For SYSLINUX, when the boot screen appears, press Tab, and type a space. Type `toram` and press Enter. 
	* For GRUB, when the boot screen appears, press `e` and use the keyboard arrows to move to the end of the line that starts with `linux`. The line is probably wrapped and displayed on multiple lines, but it is a single configuration line. Type `toram` and press F10 or Ctrl+X. 
	* You can eject the Tails USB at the beginning of your session before you do anything else (whether it is connecting to the Internet or plugging in another USB) and then still use it like normal.

On a USB with a write-protect switch, you will not be able to make any changes to the Tails USB when the switch is locked. If you can make changes, so can malware. While it would be ideal to leave the switch locked all the time, we recommend two cases where the switch must be unlocked:

1) **For a dedicated upgrade session.** If you need to upgrade Tails, you can do so in a dedicated session with the switch unlocked - this is necessary because the upgrade needs to be written to the Tails USB. Once you are done, you should restart Tails with the switch locked.
2) **If you decide to use Persistent Storage, for occasional configuration sessions.** [Persistent Storage](/posts/tails/#optional-create-and-configure-persistent-storage) is a Tails feature that allows data to persist between sessions that would otherwise be amnesiac on the Tails USB itself. Because it requires writing to the Tails USB to persist data, it is generally impractical to use with a write-protect switch. However, it may be acceptable to disable the switch for occasional Persistent Storage configuration sessions, such as installing additional software. For example, in an 'unlocked' session, you enable additional software for persistence and install Scribus, selecting to install it every session. Then, in a 'locked' session, you actually use Scribus - none of the files you work on are saved to the Tails USB because it is 'locked'. The Persistent Storage feature is not possible with the `toram` boot or with a DVD.

Where can we store personal data for use between Tails sessions if the write-protect switch prevents us from using Persistent Storage? We recommend storing personal data on a second LUKS USB. This "personal data" USB should not look identical to your Tails USB to avoid confusion. To create this separate USB, see [How to create an encrypted USB](/posts/tails/#how-to-create-an-encrypted-usb). If you are reading this from a country like the UK, where not providing encryption passwords can land you in jail, this second drive should be an HDD containing a [Veracrypt Hidden Volume](https://www.veracrypt.fr/en/Hidden%20Volume.html) (SSD and USB drives are [not suitable for Hidden Volumes](https://www.veracrypt.fr/en/Trim%20Operation.html)).

![](luks.png)

Compartmentalization is an approach that neatly separates different identities by using separate Tails sessions for separate activities - in Tails session #1 you do activities related to moderating a website, and in Tails session #2 you do activities related to researching for an action. This approach also comes into play with your "personal data" USBs. If the files you save could be used to link your activities together, use a different "personal data" USB for each activity. For a "personal data" USB that stores very sensitive files (such as the text of a communique), it is best to reformat and then destroy the USB once you no longer need the files (see [Really delete data from a USB drive](/posts/tails/#really-delete-data-from-a-usb)). This is another reason to use a separate USB for any files that need to be saved - you don't accumulate the forensic history of all your files on your Tails Persistent Storage, and you can easily destroy USBs as needed.

Finally, a note about email - if you already use Tails and encrypted email ([even though it is not very secure](/posts/e2ee/#pgp-email)), you may be familiar with Thunderbird's Persistent Storage feature. This feature allows you to store your Thunderbird email account details, as well as your inbox and PGP keys, on a Tails USB. With a "personal data" USB, Thunderbird won't automatically open your accounts. We recommend that you do one of the following:

- Create new Thunderbird email accounts in each session. PGP keys can be stored on the separate 'personal data' USB like any other file, and imported when needed. This has the advantage that if law enforcement manages to bypass LUKS, they still don't have your inbox without knowing your email password.
- Keep the Thunderbird data folder on the "personal data" USB. After logging in to Thunderbird, use the Files browser (Applications → Accessories → Files) and enable the "Show hidden files" setting. Navigate to Home, then copy the folder called `.thunderbird` to your "personal data" USB.  In each future session, after you have unlocked the 'personal data' USB and before you start Thunderbird, copy the `.thunderbird` folder to Home (which is running in RAM, so doesn't require the write-protect switch to be unlocked). 

Another reason to avoid using Persistent Storage features is that many of them persist user data to the Tails USB. If your Tails session is compromised, the data you access during that session can be used to tie your activities together. If there is user data on the Tails USB, such as an email inbox, compartmentalization of Tails sessions is no longer possible. To achieve compartmentalization with Persistent Storage enabled, you would need a dedicated Tails USB for each identity, and updating them all every month would be a lot of work. 

# Encryption  

## Passwords

[Encryption](/glossary#encryption) is a blessing—it's the only thing standing in the way of our adversaries reading all our data, if it's used well. The first step in securing your encryption is to make sure that you use very good passwords—most passwords don't need to be memorized because they are stored in a password manager called KeePassXC, so they can be completely random. To learn how to use KeePassXC, see [Password Manager](/posts/tails/#password-manager-keepassxc). 

>In the terminology used by KeePassXC, a [*password*](/glossary/#password) is a random sequence of characters (letters, numbers and other symbols), while a [*passphrase*](/glossary/#passphrase) is a random sequence of words. 

Never reuse a password/passphrase for multiple things ("password recycling") - KeePassXC makes it easy to store unique passwords that are dedicated to one purpose. [LUKS](/glossary/#luks) encryption **is only effective when the device is powered off** - when the device is powered on, the password can be retrieved from memory. Any encryption can be [brute-force attacked](/glossary#brute-force-attack) with [massive amounts of cloud computing](https://blog.elcomsoft.com/2020/08/breaking-luks-encryption/). The newer version of LUKS (LUKS2 using Argon2id) is [less vulnerable to brute-force attacks](https://mjg59.dreamwidth.org/66429.html); this is the default as of Tails 6.0 ([forthcoming](https://gitlab.tails.boum.org/tails/tails/-/issues/19733)) and Qubes OS 4.1. If you'd like to learn more about this change, we recommend [Systemli's overview](https://www.systemli.org/en/2023/04/30/is-linux-hard-disk-encryption-hacked/) or [dys2p's](https://dys2p.com/en/2023-05-luks-security.html). 

Password strength is measured in "[bits of entropy](https://en.wikipedia.org/wiki/Password_strength#Entropy_as_a_measure_of_password_strength)". Your passwords/passphrases should ideally have an entropy of about 128 bits (diceware passphrases of **ten words**, or passwords of **21 random characters**, including uppercase, lowercase, numbers, and symbols) and shouldn't have less than 90 bits of entropy (seven words).

![](passphrase.png)

What is a diceware passphrase? As [Privacy Guides notes](https://www.privacyguides.org/en/basics/passwords-overview/#diceware-passphrases), "Diceware passphrases are a great option when you need to memorize or manually input your credentials, such as for your password manager's master password or your device's encryption password. An example of a diceware passphrase is `viewable fastness reluctant squishy seventeen shown pencil`." The Password Generator feature in KeePassXC can generate diceware passphrases and random passwords. If you prefer to generate diceware passphrases using real dice, see [Privacy Guides](https://www.privacyguides.org/en/basics/passwords-overview/#diceware-passphrases).

Our recommendations are:

1) Memorize diceware passphrases of 7-10 words for everything that is not stored in a KeePassXC database.
2) Generate passwords of 21 random characters for everything that can be stored in a KeePassXC database. Maintain an offsite backup of your KeePassXC database(s) in case it is ever corrupted or seized. 

> **Tip** 
>
> Diceware passphrases can be easy to forget if you have several to keep track of, especially if you use them infrequently. To reduce the risk of forgetting a diceware passphrase, you can create a KeePassXC file with all "memorized" passphrases in it. Store this on a LUKS USB, and hide that USB somewhere off-site where it won't be recovered in a police raid. You should be able to reconstruct both the LUKS and KeePassXC passphrases if a lot of time has passed. One strategy is to use a memorable sentence from a book - this reduction in password entropy is acceptable if the USB is highly unlikely to ever be recovered due to its storage location. That way, if you ever really forget a "memorized" passphrase, you can access that offsite backup. As with all important backups, you should have at least two. 

For Tails, you need to memorize two passphrases:

1) The [LUKS](/glossary/#luks) "personal data" USB passphrase, where your KeePassXC file is stored.
2) The KeePassXC passphrase

If you are using Persistent Storage, this is another passphrase that you will have to enter on the Welcome Screen at boot time, but it can be the same as 1. 

## Encrypted containers 

[LUKS](/glossary#luks) is great, but defense-in-depth can't hurt. If the police seize your USB in a house raid, they will try a [variety of tactics to bypass the authentication](https://www.csrc.link/threat-library/techniques/targeted-digital-surveillance/authentication-bypass.html), so a second layer of defense with a different encryption implementation can be useful for highly sensitive data. 


[Gocryptfs](https://nuetzlich.net/gocryptfs/) is an encrypted container program that is [available for Debian](https://packages.debian.org/bullseye/gocryptfs) and can be easily installed as [additional software](/posts/tails/#optional-create-and-configure-persistent-storage). If you don't want to reinstall it every session, you will need to [configure Additional Software in Persistent Storage](#using-a-write-protect-switch). 

To use gocryptfs, you will need to use Terminal (the [command line](/glossary#command-line-interface-cli)).

On your Personal Data LUKS USB, use the file manager to create two folders and name them `cipher` and `plain`. Right click in the white space of your file manager and select 'Open Terminal Here'. This will allow you to be in the correct location when Terminal opens, instead of having to know how to navigate using the `cd` command. 

In Terminal, use the `ls` command to list the folders you have, and it should output the two you just created, among others:

`ls`

The first time you use Gocryptfs, create a Gocryptfs filesystem;

`gocryptfs -init cipher`

You will be prompted for a password. Create a new entry in your KeepassXC file and generate a password using the Generate Password feature (the dice icon). Then copy the password and paste it into the terminal (Edit → Paste or Ctrl+Shift+V). It will output a master key—save it in the KeepassXC entry. 

Every time you use the filesystem, mount it like this:

`gocryptfs cipher plain`

You will be prompted for the password. Note that the order is important - `cipher` is the first argument and `plain` is the second. 

You can now add files to your mounted, decrypted container in the 'plain' folder. When you unmount the filesystem, the container will be encrypted. To do this:

`fusermount -u plain`

Now plain is just an empty folder again. Before storing important files in the container, you should run a test to make sure it works as expected, especially if you are unfamiliar with the command line interface.  

## Encrypted Communication 

PGP email is the most established form of encrypted communication on Tails in the anarchist space. Unfortunately, PGP does not have [forward secrecy](/glossary#forward-secrecy)—that is, a single secret (your private key) can decrypt all messages, rather than just a single message, which is the standard in encrypted messaging today. It is the opposite of "metadata protecting", and has [several other shortcomings](/posts/e2ee/#pgp-email). 

For [synchronous](/glossary/#synchronous-communication) and [asynchronous](/glossary/#asynchronous-communication) messaging we recommend [Cwtch](/posts/e2ee/#cwtch). For more information on Cwtch, see [Encrypted Messaging For Anarchists](/posts/e2ee/). 

# Phishing Awareness 

Finally, consider how an adversary would conduct a [remote attack](/glossary/#remote-attacks) targeting you or your project; the answer is most likely ["phishing"](/glossary/#phishing). *Phishing* is when an adversary crafts an email (or text, message in an application, etc.) to trick you into revealing information, gain access to your account, or introduce malware to your machine. [*Spear phishing*](/glossary/#spear-phishing) is when the adversary has done some reconnaissance and uses information they already know about you to tailor their phishing attack. 

You have probably heard the advice to be skeptical about clicking on links and opening attachments—this is why. To make matters worse, the "from" field in emails can be spoofed to fool you—[PGP signing](/posts/e2ee/#pgp-email) mitigates this to prove that the email is actually from who you expect it to be from. 

Sometimes the goal of phishing is to deliver a "payload" that calls back to the adversary—it is the [initial access](https://attack.mitre.org/tactics/TA0001/) entry point to infect your machine with malware. A payload can be embedded in a file and run when the file is opened. In the case of a link, a payload can be delivered via malicious JavaScript in the website, allowing the payload to be executed on your computer. Tor is supposed to protect your location (IP address), but now the adversary has a way to further their attack; [make the infection persistent](https://attack.mitre.org/tactics/TA0003/), [install a screen or key logger](https://attack.mitre.org/tactics/TA0009/), [exfiltrate your data](https://attack.mitre.org/tactics/TA0010/), etc. The reason Tails does not have a default Administration password (it must be set on the session's Welcome Screen if needed) is to make it more difficult to [escalate privileges](https://attack.mitre.org/tactics/TA0004/), which would be necessary to bypass Tor. 

## Attachments 

For untrusted attachments, you would ideally **sanitize all files sent to you before opening them** with a program like [Dangerzone](https://dangerzone.rocks/), which takes potentially dangerous PDFs, office documents, or images and converts them into safe PDFs. Unfortunately, Dangerzone is [not yet readily available in Tails](https://gitlab.tails.boum.org/tails/tails/-/issues/18135). An inferior option is to **open untrusted files in a dedicated ['offline mode'](https://tails.boum.org/doc/first_steps/welcome_screen/index.en.html#index3h2) session**, so that if they're malicious they can't call home, and shut the session down immediately afterward, minimizing their chance of persistence. Tails prevents deanonymization through phishing by forcing all internet connections through the Tor network. However, this is still vulnerable to [0-day exploits](/glossary#zero-day-exploit) that nation-state actors have access to. For example, the FBI and Facebook worked together to develop a 0-day exploit against Tails [that deanonymized a user](https://www.vice.com/en/article/v7gd9b/facebook-helped-fbi-hack-child-predator-buster-hernandez) after he opened a video attachment from his home Wi-Fi.

## Links 

With untrusted links, there are two things you must protect: your anonymity and your information. Unless the adversary has a 0-day exploit on the Tor Browser or Tails, your anonymity should be protected **if you don't enter any identifying information into the website**. Your information can only be protected **by your behavior**—phishing awareness allows you to think critically about whether this could be a phishing attack and act accordingly.

Investigate untrusted links before you click by **manually copying and pasting the address into your browser**—do not click through a hyperlink as the text can be used to mislead you about where you are going. **Never follow a shortened link** (e.g. a site like bit.ly that takes long web addresses and makes a short one) because it cannot be verified before redirection. [Unshorten.me](https://unshorten.me/) can reveal shortened links. 

![](duckduck.cleaned.png)

Also, **don’t follow links to domains you don't recognize**. When in doubt, search for the domain with the domain name in quotation marks using a privacy-preserving search engine (such as DuckDuckGo) to see if it’s a legitimate website. This isn’t a surefire solution, but it’s a good precaution to take.

Finally, if you click on any link in an email and are asked to log in, be aware that this is a common endgame for phishing campaigns. **Do not do it**. Instead, manually go to the website of the service you are trying to access and sign in there. That way, you’ll know you’re logging in to the right site because you’ve typed in the address yourself, rather than having to trust the link in the email. For example, you might type your password at mailriseup.net instead of mail.riseup.net (this is called "typo-squatting"). Similarly, a "[homograph attack](https://www.theguardian.com/technology/2017/apr/19/phishing-url-trick-hackers)" substitutes Cyrillic letters for normal letters, which is even harder to visually recognize.

You may want to open untrusted links in a dedicated Tails session without unlocked Persistent Storage or attaching "personal data" USBs. 

# To Conclude

Using Tails without any of this advice is still a vast improvement over many other options. Given that anarchists regularly entrust their freedom to Tails, such as sending communiques, taking these extra precautions can further strengthen your trust in this operating system. 

# Appendix: Deanonymization of your WLAN (Wi-Fi) adapter despite Tails?

***Capulcu*** *(from [Autonomes Blättchen No. 49](https://autonomesblaettchen.noblogs.org/files/2022/06/nr49web.pdf), 2022)*

The two main techniques for anonymizing network traffic while using Tails are using Tor to obfuscate IP addresses and using a MAC address changer to obfuscate the MAC address. In theory, this does the trick. However, security cannot always be guaranteed and attacks aimed at deanonymization occur against both techniques. The compromise of one technique does not entail the compromise of the other. Nevertheless, *for particularly sensitive publications*, it is important to thwart all possibilities of successful identification.

**Background information:** The IP address can be used to identify the location of the router. The MAC address is 'only' used for local assignment: which endpoint device is to receive which data packet from the router. According to the current Internet standard, it is not usually sent beyond the router to the Internet[^1].

![](diagram.png)

In September 2019, our collective published a short statement ("[Security warning about MAC changer](https://capulcu.blackblogs.org/)") in which we warn against possible deanonymization through the use of WLAN adapters - including when using the Tails operating system. Here, we want to supplement the chapter "Dangers of WLAN adapters" in the current edition of the [Capulcu Tails publication](https://capulcu.blackblogs.org/wp-content/uploads/sites/54/2021/04/Tails-2021-04-12.pdf) with insight into the problems of WLAN adapters and a recommendation for use.

**The problem:** WLAN adapters send manufacturer-specific information with the data transfer. This information can enable a unique assignment despite a MAC address spoofed by the MAC changer. **This affects both internal WLAN adapters that are installed in your laptop in the form of a network card, as well as external WLAN adapters connected via USB**. The technical details are explained below. This fingerprinting is not conclusive forensic evidence. In combination with other evidence, however, it could result in a legally constructed 'unique' assignment: which computer was responsible for a certain Internet publication.

**A concrete example**: Due to previous police surveillance, a café in your city is suspected of being used for the publication of communiques. The café operator has allowed himself to be bribed or coerced by the cops into configuring his (commercially available) Internet router in such a way that it logs all of the data packets of all computers seeking contact. If the presence of various laptops in this café was 'recorded' at the same time as an explosive Indymedia publication, this could be used for further investigations, despite the fact that the content of the data packets only shows that the data was anonymized using Tor. If your computer was logged (despite a spoofed MAC address) and if the fingerprint of your WLAN adapter turns up again elsewhere (by chance, or through targeted investigations - e.g. during a house raid) and can be proven as belonging to you, a prosecutor could try to use this as evidence of you submitting the Indymedia publication.

**Recommendation**: Until there is a (stable) solution for the "WLAN fingerprinting" problem, you should remove the internal WLAN adapter for particularly sensitive research and publications and use a (cheap) external USB WLAN adapter and **dispose of it after use**. We also advise you to use WLAN adapters that can be controlled by the Tails operating system without manufacturer-specific firmware (e.g. WLAN adapters with Qualcomm's Atheros chip that use the ath9k driver).

## Description of the problem and technical details

If you have not explicitly deactivated the WLAN on the Tails welcome screen (via Offline Mode) or, if available, via a hardware switch, the Tails operating system will automatically search for existing WLAN access provided by access points (Wi-Fi routers). It does this by sending a radio signal (*probe request*) at regular intervals to all access points in the vicinity. The regularly sent request contains the unique MAC address of your WLAN adapter. However, Tails protects your anonymity by not sending the real address, but a randomly generated MAC address. If there are access points in the vicinity, they also respond with a radio signal (*probe response*). This response contains information about the network name (SSID), authentication and encryption. The information contained in these radio signals makes it possible to connect to an access point and exchange data packets.

The problem: Various studies from the years 2016-2019, whose results are recorded in various publications, show that radio signals also contain other information that can be used to identify you with a high degree of probability despite a changed MAC address!

The paper "[*Why MAC Address Randomization is not Enough: An Analysis of Wi-Fi Network Discovery Mechanisms*](https://papers.mathyvanhoef.com/asiaccs2016.pdf)" shows the possibility of an identification based on radio signals (probe requests) by the WLAN standard [802.11](https://en.wikipedia.org/wiki/IEEE_802.11), which is also used by Tails. Here, the (spoofed) MAC addresses are disregarded and deanonymization takes place via the radio signals sent by WLAN adapters (via so-called "probe request fingerprinting"). The paper refers to real-world test data, i.e. with data from commercially available hardware[^2], and shows that WLAN radio signals contain enough information to uniquely identify their specific patterns. The paper also discusses various attack options for deanonymization, which we will not summarize here.

The paper "[*A Study of MAC Address Randomization in Mobile Devices and When it Fails*](https://arxiv.org/pdf/1703.02874)" takes the previous study as a starting point and adds further possibilities for identifying endpoint devices with changed MAC addresses. The study concludes that MAC address modification can be overridden by the attacks presented and is not sufficient for anonymization. The authors suggest to change the entire MAC address and not only the digits after the manufacturer identifier - the so-called OUI[^3], as [is the case with Tails](https://tails.boum.org/contribute/design/MAC_address/#active-probe-fingerprinting). In addition, according to the paper, a random MAC address should be used for each separate probe request.

Another paper titled "[*Defeating MAC Address Randomization Through Timing Attacks*](http://papers.mathyvanhoef.com/wisec2016.pdf)" deals with probe requests and the detection of devices that change their MAC addresses at periodic intervals (which does not happen under Tails and is fatal according to the paper). In the summary, the authors of the paper conclude that the attack they use can deanonymize a large fraction of devices (up to 77%), even if no large amounts of data are transmitted in the radio signals.

Further publications on possible deanonymization attacks (which do not explicitly affect Linux operating systems) can be found here:

- "[Know Thy Quality: Assessment of Device Detection by WiFi Signals](http://sig-iss.work/percomworkshops2019/papers/p639-rutermann.pdf)"
- "[Accurate and Efficient Wireless Device Fingerprinting Using Channel State Information](https://www.cs.ucr.edu/~zhiyunq/pub/infocom18_wireless_fingerprinting.pdf)" 
- "[Fingerprinting 802.11 Implementations via Statistical Analysis of the Duration Field](http://www.uninformed.org/?v=5&a=1&t=pdf)"
- "[Device Fingerprinting in Wireless Networks: Challenges and Opportunities](https://arxiv.org/pdf/1501.01367v1.pdf)" 

## Probe Request Fingerprinting

The probe requests sent at short intervals by all WLAN adapters (whether internal or external) contain WLAN adapter-specific information elements (IEs) in the management frame. The values of the [IEs](http://download.aircrack-ng.org/wiki-files/other/managementframes.pdf) are partly manufacturer-specific (in terms of content and sequence). This makes them particularly suitable for deanonymizing fingerprinting, which was used in the previously mentioned papers. Among the various implementations of proprietary [WLAN firmware](https://en.wikipedia.org/wiki/Proprietary_software), there are so many different ways to arrange them that tracking can therefore be successful. In addition, WLAN adapters can often be distinguished by sequence number[^4], data throughput rate, and other radio signal-specific parameters[^5].

## Reduce the digital footprint

The packet sizes of probe requests differ according to the information they contain. In most cases, this depends heavily on the firmware implementations of the manufacturers. However, there are also free driver implementations for WLAN adapters that do not require proprietary firmware and can be controlled via the operating system[^6]:

>[ath9k](https://wiki.debian.org/ath9k) is a Linux kernel driver supporting Atheros 802.11n PCI/PCI-E chips, introduced at Linux 2.6.27. It does not require a binary HAL (hardware abstraction layer) and no firmware is required to be loaded from userspace. 

This gives you control over your WLAN adapter and already reduces your digital footprint (e.g. ath9k WLAN adapter drivers do not contain vendor specific tags). This is also noticeable in the reduced packet size of probe requests[^7]. On the Wikipedia page for the comparison of [open source WLAN drivers](https://en.wikipedia.org/wiki/Comparison_of_open_source_wireless_drivers) you can find other hardware besides ath9k WLAN adapters that does not need vendor specific firmware[^8].

After our warning in summer 2019, we summarized our ideas for avoiding probe requests and listening for probe responses in a [proposal for improving the Tails operating system](https://gitlab.tails.boum.org/tails/tails/-/issues/17831). In it, we suggest replacing network software on Debian (which provides the basis for Tails) with newer applications in which periodic scanning for access points can be disabled. In our tests, this made it possible to passively find access points and establish a connection without probe requests. These considerations were initially [rejected by the Tails developers](https://gitlab.tails.boum.org/tails/tails/-/issues/6453), since a software we used (iwd) is still too unstable in their eyes.

*capulcu*


# Appendix 2: Location, Location, Location

*From **How to Hack like a Ghost** by Sparc Flow, available on [Library Genesis](https://en.wikipedia.org/wiki/Library_Genesis)*

One way to increase your anonymity is to be careful of your physical location when hacking. Don’t get me wrong: Tor is amazing. [...] But when you do rely on these services, always assume that your IP address—and hence, your geographical location and/or browser fingerprint—is known to these intermediaries and can be discovered by your final target or anyone investigating on their behalf. Once you accept this premise, the conclusion naturally presents itself: to be truly anonymous on the internet, you need to pay as much attention to your physical trail as you do to your internet fingerprint.

If you happen to live in a big city, use busy train stations, malls, or similar public gathering places that have public Wi-Fi to quietly conduct your operations. Just another dot in the fuzzy stream of daily passengers. However, be careful not to fall prey to our treacherous human pattern-loving nature. Avoid at all costs sitting in the same spot day in, day out. Make it a point to visit new locations and even change cities from time to time.

Some places in the world, like China, Japan, the UK, Singapore, the US, and even some parts of France, have cameras monitoring streets and public places. In that case, an alternative would be to embrace one of the oldest tricks in the book: war driving. Use a car[^9] to drive around the city looking for public Wi-Fi hotspots. A typical Wi-Fi receiver can catch a signal up to 40 meters (~150 feet) away, which you can increase to a couple hundred meters (a thousand feet) with a directional antenna, like Alfa Networks' Wi-Fi adapter. Once you find a free hotspot, or a poorly secured one that you can break into—WEP encryption and weak WPA2 passwords are not uncommon and can be cracked with tools like Aircrack-ng and Hashcat— park your car nearby and start your operation. If you hate aimlessly driving around, check out online projects like [WiFi Map](https://www.wifimap.io) that list open Wi-Fi hotspots, sometimes with their passwords.

Hacking is really a way of life. If you are truly committed to your cause, you should fully embrace it and avoid being sloppy at all costs.

<br>

<hr>

[^1]: This applies to the IPv4 Internet protocol standard. Caution: In some company networks, this no longer applies! 

[^2]: Eight million Probe Requests, most of which were collected from a busy square in Rome and a train station in Lyon.

[^3]: According to the Tails developers, unusual MAC addresses also stand out and are therefore not used by Tails.

[^4]: Tails does not change the MAC address after a random number of probe requests, nor does it reset the sequence number of transmitted packets, which provides additional tracking.

[^5]: HT Capabilities, Supported Rates, Extended Supported Rates, Extended Capabilities, VHT Capabilities, Vendor Specific,...

[^6]: More precisely: via the kernel.

[^7]: The smaller the packet size, the fewer traces there are as well.

[^8]: Recognizable by the green fields in the column "Non-free firmware required."

[^9]: AnarSec note: Keep in mind that a car can easily be [tracked with a GPS device](https://www.csrc.link/threat-library/techniques/covert-surveillance-devices/location.html). 
