+++
title = "Contact"
sort_by = "date"
paginate_by = 5
+++
<br>

<div class="has-text-centered">
<p>

**Matrix**: @anarsec:riot.anarchyplanet.org 

[What is Matrix?](/posts/e2ee/#element-matrix)

**Email**: anarsec (at) riseup (dot) net 

[PGP key](/anarsec.asc) 

[Why we don't recommend email](/posts/e2ee/#pgp-email)

>Our PGP public key can be verified from a second location [at 0xacab](https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc) - commit history should display "Initial commit". 
>
>WayBack Machine of PGP key: [anarsec.guide](https://web.archive.org/web/20230619164601/https://www.anarsec.guide/anarsec.asc) / [0xacab.org](https://web.archive.org/web/20230619164309/https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc)


# Contribute

Anarsec encourages contributions! If you would like to suggest edits to a guide, either use the contact information listed above, or submit an issue or merge request on [Riseup's Gitlab instance](https://0xacab.org/anarsec/anarsec.guide) - whatever is your preference.  

We are also open to submitted guides - please get in touch with proposals. 

>All 0xacab commits are signed with SSH key fingerprint: 
xXfPe+zku+SaJorO4XldMFcAVPMmQQgLHl4VpmYhiok 

